package pl.cbartek;

public interface CashMachineInterface {

	public void deposit(int amount);
	public void withdraw(int amount);
	public void accountBalance();
	
}
