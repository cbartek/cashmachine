package pl.cbartek;

import java.util.concurrent.TimeUnit;

public class CashMachineImpl implements CashMachineInterface {

	Money money = new Money(1000, "PLN");
	
	public void wait2Seconds(){
		try {
			TimeUnit.SECONDS.sleep(2);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	@Override
	public void deposit(int amount) {
		System.out.println("\nWpłacasz: " + amount);
		money.setAmount(money.getAmount()+amount);
		wait2Seconds();

	}

	@Override
	public void withdraw(int amount) {
		if(amount>money.getAmount()) {
			System.out.println("Nie masz wystarczającej ilości pieniędzy");
			wait2Seconds();
		}else {
			System.out.println("\nWypłacasz: " + amount);
			money.setAmount(money.getAmount()-amount);
			wait2Seconds();
		}
	}

	@Override
	public void accountBalance() {
		System.out.println("\nMasz na koncie: " + money.getAmount());
		wait2Seconds();
		
		
	}

}
