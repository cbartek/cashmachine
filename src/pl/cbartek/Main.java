package pl.cbartek;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		CashMachineImpl cashMachine = new CashMachineImpl();
		int amount;
		Menu menu1= new Menu();
		
		int choose = menu1.menu();
	
		while(choose!=0) {
			switch(choose) {
			case 1:
				System.out.print("Podaj kwote do wpłaty: ");
				amount=in.nextInt();
				cashMachine.deposit(amount);
				System.out.println();
				choose = menu1.menu();
				break;
			case 2: 
				System.out.print("Podaj kwote do wypłaty: ");
				amount=in.nextInt();
				cashMachine.withdraw(amount);
				System.out.println();
				choose = menu1.menu();
				break;
			case 3:
				cashMachine.accountBalance();
				choose = menu1.menu();
				break;
			default:
				System.out.print("Błedna funkcja");
				choose = menu1.menu();
				break;
			}
			
		}
		
		
	}

}
