package pl.cbartek;

import java.util.Scanner;

public class Menu {

	public static int menu() {
		System.out.println();
	    System.out.println("     ****************************************");
	    System.out.println("     *                 MENU                 *");
	    System.out.println("     ****************************************");
	    System.out.println("     1. Wpłata");
	    System.out.println("     2. Wypłata");
	    System.out.println("     3. Stan Konta");
	    System.out.println("     0. Koniec");
	    
	    Scanner in = new Scanner(System.in);
        int w = in.nextInt();
 
        return w;
	}
	
}
